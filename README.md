# BMLT NA Meeting Search iOS App #

This is a comprehensive meeting search app for iPhone and iPad, based on [the BMLTiOSLib iOS Framework](https://bmlt.magshare.net/specific-topics/bmltioslib/).

Yes, the damn repo is labeled "NA Meeting Finder." That's my fault. "Search" is more comprehensive, so the final app will be called "NA meeting Search" (If the Powers That be will it).

It is provided by [the BMLT developers](https://bmlt.magshare.net).

It uses [the worldwide version](https://na-bmlt.org/_/sandwich/) of [the Sandwich server](http://archsearch.org/sandwich/) to locate meetings, using [the BMLTiOSLib iOS Framework](https://bmlt.magshare.net/specific-topics/bmltioslib/).
The project also uses [the excellent SwipeableTabBarController Project by Marco Griselli](https://github.com/marcosgriselli/SwipeableTabBarController). It does not use it as a [CocoaPod](https://cocoapods.org). Instead, it simply uses the files directly _(This is because we don't want to require that implmentors install the CocoaPods system -which is very nice, but an extra complication. As a result of this restriction, we've made a couple of very small -and clearly marked- modifications to the Swipeable TabBarController project to make it work in our environment.)_

### This Repository Is 100% of the Source Code for This Project ###

* Like all [BMLT](https://bmlt.magshare.net)  projects, this is a completely open-source project. There is no hidden or proprietary code anywhere.

### Setup ###

* This is an [Apple Xcode](https://developer.apple.com/xcode/) project, using [Swift](https://developer.apple.com/swift/) language, version 4. It requires Xcode Version 9, at minimum.
* The project requires [the BMLTiOSLib iOS Framework](https://bmlt.magshare.net/specific-topics/bmltioslib/). If you include the [Git repo for this project](https://bitbucket.org/bmlt/bmlt-na-meeting-finder), you will have the BMLTiOSLib included as a [Git Submodule](https://git-scm.com/book/en/v2/Git-Tools-Submodules).
* This will require that the user have iOS version 10.0 or greater.

### License ###

This is a [GPL V.3](https://gnu.org) project.

This is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

BMLT is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See [the GNU General Public License](https://www.gnu.org/licenses/licenses.html#GPL) for more details.

## CHANGELIST ##
***Version 1.1.0.2006* ** *- January 30, 2018*

- Added a placeholder for Italian localization (It has not yet actually been localized).

***Version 1.1.0.2005* ** *- January 26, 2018*

- If you do a search from the Location/Map screen, the initial results tab is the map. The other two result in list results as the initial screen.

***Version 1.1.0.2004* ** *- January 19, 2018*

- The map results now show the user location.

***Version 1.1.0.2003* ** *- January 7, 2018*

- Updated to the latest version of the BMLTiOSLib pod.
- Tweaked the sort for weeks that start on days other than Monday.

***Version 1.1.0.2002* ** *- January 7, 2018*

- Updated to the latest version of the BMLTiOSLib pod.
- Added the Reveal Framework.
- Added initial Swedish localization.

***Version 1.1.0.2001* ** *- December 13, 2017*

- Updated to the latest version of the BMLTiOSLib pod.

***Version 1.1.0.2000* ** *- December 12, 2017*

- Added the BMLTiOSLib and Swipeable Tab Bar Controller as CocoaPods, as well as SwiftLint, and cleaned up the code as per SwiftLint.

***Version 1.0.0.3000* ** *- September 21, 2017*

- First App Store Release

***Version 1.0.0.2006* ** *- September 19, 2017*

- Made the label for the "Use Google Maps" switch shrink its text size if necessary.

***Version 1.0.0.2005* ** *- September 19, 2017*

- Fairly significant UI changes. Made the backgrounds dark blue, enhanced the contrast on the Tab Bar and made the buttons look more like "buttons."

***Version 1.0.0.2001* ** *- September 15, 2017*

- Fixes a crash that can occur if a long search is in progress.

***Version 1.0.0.2000* ** *- September 13, 2017*

- First Beta release.

